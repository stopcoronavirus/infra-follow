# Introduction


[www.stopcoronavirus.tech](https://www.stopcoronavirus.tech) is setup using [IPFS](https://ipfs.io/) as its base layer, combined with some services that can be run by anyone to help distribute, relay and store information and data.

It has been set up to be as decentralized as possible.

# Architecture Overview

It relies on :

- a [dedicated IPFS network](https://gitlab.com/stopcoronavirus/infra-follow/-/blob/master/docker-compose.yml#L53) (swarm.key file is published in this repo so anyone can join) : we have less nodes than the public IPFS Network but nodes are more connected
- Gitlab.com for hosting [repos](https://gitlab.com/stopcoronavirus/), and deploying ~~a backup of~~ static content
- ~~a [custom Gitlab instance](https://gitlab.stopcoronavirus.tech), mirroring Gitlab.com repos, deploying the services and their static content (main website, p2p chat, and p2p pad)~~
- DNS (can be removed : we could rely on our dedicated IPFS network or the public IPFS network for hosting static contents, but we will still need a IPFS Gateway so anyone can access via current web browsers)
- a [rendezvous server](https://gitlab.com/stopcoronavirus/infra-follow/-/blob/master/docker-compose.yml#L81) (acting as a relay for p2p chat and p2p pad)
- a [pinner service](https://gitlab.com/stopcoronavirus/infra-follow/-/blob/master/docker-compose.yml#L103) so that content published and exchanged under the p2p pad ([peer-pad](https://github.com/peer-base/peer-pad)) is persisted on the dedicated IPFS network.

# Deploying your node, and joining the decentralized infrastructure

This tutorial assumes you have a VPS or dedicated server running.  
It has been tested with ubuntu:18.04.

As written, it deploys multiple endpoints under the subdomain ```*.bootstrap.stopcoronavirus.tech```

You can deploy your node for being part of the bootstrap list of nodes or just to add capacity


**Deploying a bootstrap node**  

If you'd like us to add your node as a bootstrap node, you can either :

- use this subdomain ```*.bootstrap.stopcoronavirus.tech``` and ask us to add the proper DNS record to point to your server (see STEP 6)
- use your own subdomain ```*.bootstrap.yourdomain.ext``` and only ask us to add your node in the bootstrap list

In both cases, it is needed to update configuration in multiple repos because the bootstrap list will change.

**Joining as a peer node**  

Otherwise, you can also deploy your node without your server being part of the bootstrap list.  
In this case, your server join the network and stills add capacity, and you don't need a domain name.  
And, you won't need to submit a pull request.  

Feel free to adapt this tutorial : you should be able to remove the following services under ```docker-compose.yml``` : ```ipfs-gateway-wss```, ```wss-star-rdv```. No request should reach them if they're not part of the bootstrap list.


## Step 1 : SSH to your server

## Step 2 : Install Docker on your server

_adapted from official documentation for [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/)_

```
apt-get update

apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
   
apt-get update

apt-get install -y docker-ce docker-ce-cli containerd.io
apt install -y docker-compose

reboot
```

## Step 3 : Clone this repo

```
git clone https://gitlab.com/stopcoronavirus/infra-follow.git
cd infra-follow
```

## Step 4 : Edit SERVER_NAME under .env file

_this steps matters if you'd like to add your node to the bootstrap list_
Please use this convention to name your server : ```<GITHUB_OR_GITLAB_USERNAME>_<2_DIGITS_SERVER_NB>```

Example : ```stopcoronavirus_02```

Once you have setup your ```SERVER_NAME```, you can decide to use your subdomain ```*.bootstrap.stopcoronavirus.tech``` or your own before requesting to be added to the bootstrap list.

Find & replace any occurence of ```bootstrap.stopcoronavirus.tech``` in this repo accordingly.

Please note however that you won't be able to finalize the setup of your server using our subdomain ```*.bootstrap.stopcoronavirus.tech``` unless we add a DNS record to your server. Please contact [dev@stopcoronavirus.tech](mailto:dev@stopcoronavirus.tech)

## Step 5 : Configure Nginx

### Generate Nginx configuration

```
source .env && export SERVER_NAME
./ipfs-gateway-wss/ipfs-gateway-wss.conf.tmpl > ipfs-gateway-wss/conf.d/ipfs-gateway-wss.${SERVER_NAME}.bootstrap.stopcoronavirus.tech.conf
```

### Check Nginx configuration

You should see domains containing your chosen ```SERVER_NAME```

```
cat ipfs-gateway-wss/conf.d/ipfs-gateway-wss.${SERVER_NAME}.bootstrap.stopcoronavirus.tech.conf
```

### Apply configuration and restart Nginx container

_Not needed the first time. You can skip this step as docker containers are not launched yet._

```
docker-compose exec nginx-proxy /bin/bash -c 'nginx -t && nginx -s reload'
docker restart nginx-proxy
```

## Setup IPFS Gateway, RendezVous Server, and Peer-Base Pinner Service

### Some aliases to help...

_not mandatory, but can be useful_

```
alias dc-s='docker-compose stop'
alias dc-sr='dc-s && docker-compose rm --force'
alias dc-sru='dc-sr && docker-compose up -d'
alias dc-srul='dc-sru && docker-compose logs -f --tail 100'
alias dc-u='docker-compose up -d'
alias dc-l='docker-compose logs -f --tail 100'
alias dc-ul='dc-u && dc-l'

alias ipfs='docker exec ipfs-gateway ipfs'
```

### Launch docker-compose recipe, configure IPFS node, reload containers

```
dc-srul

ipfs bootstrap rm --all

ipfs bootstrap add /dns4/ipfs-gateway.alpha-01.bootstrap.stopcoronavirus.tech/tcp/4001/p2p/QmXFeqdECoxEztvFfpJcTHj5zvBwfFFWEDVNu5uK5viSnb

ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://127.0.0.1:8080", "http://127.0.0.1:5001"]'
ipfs config --json API.HTTPHeaders.Access-Control-Allow-Methods '["PUT", "GET", "POST"]'

ipfs config --bool Swarm.EnableRelayHop true
ipfs config --bool Swarm.EnableAutoRelay true

ipfs config Addresses.Swarm '["/ip4/0.0.0.0/tcp/4001", "/ip4/0.0.0.0/tcp/8081/ws", "/ip6/::/tcp/4001"]' --json

dc-srul

ipfs pin add QmP48ZRbtoYQQf1pCSvEw8JgsrvRCVeemoHNMnYBzcoVyn 
```

Pinning ```QmP48ZRbtoYQQf1pCSvEw8JgsrvRCVeemoHNMnYBzcoVyn``` allows you to access your IPFS WebUI on the dedicated IPFS network.


### Connect to your remote IPFS Gateway to check if it runs properly

This will create a SSH Tunnel from your host to your server.
So you can connect to your WebUI to see if everything works as intended

```
ssh -L 8080:127.0.0.1:8080 -L 5001:127.0.0.1:5001 -N <SERVER_IP>
```

### Connect to WebUI

http://127.0.0.1:8080/ipfs/QmP48ZRbtoYQQf1pCSvEw8JgsrvRCVeemoHNMnYBzcoVyn

## Step 6a : Submit your SERVER as a bootstrap node

Skip this step and read STEP 6b if you don't want your server to be part of the bootstrap list.

### Using a DNS record we can add for simplicity

Under this setup and once the proper DNS record has been added, your server is expected to be reached at (see [TESTING.md](TESTING.md)): 

- ```rdv.${SERVER_NAME}.bootstrap.stopcoronavirus.tech```
- ```ipfs-gateway.${SERVER_NAME}.bootstrap.stopcoronavirus.tech```
- ```ipfs-gateway-wss.${SERVER_NAME}.bootstrap.stopcoronavirus.tech```
- ```pbpinner.${SERVER_NAME}.bootstrap.stopcoronavirus.tech```

Because we manage ```stopcoronavirus.tech```, we need to add a DNS Record so that ```*.${SERVER_NAME}.bootstrap.stopcoronavirus.tech``` points to YOUR SERVER.

[Please submit an issue here](https://gitlab.com/stopcoronavirus/infra-follow/-/issues/new) so we can : 

- add the proper DNS record (needed so that Let's Encrypt can check your auto-generated SSL certificates using [jwilder/nginx-proxy](https://github.com/jwilder/nginx-proxy))
- add your server as a bootstrap node (if your setup / server is always online) in the config files for [docs.stopcoronavirus.tech](https://docs.stopcoronavirus.tech) and [entraide.stopcoronavirus.tech](https://entraide.stopcoronavirus.tech)


### If you have your OWN DOMAIN or if you prefer to submit your node using its IP

[Please submit an issue here](https://gitlab.com/stopcoronavirus/infra-follow/-/issues/new).  
You can also contact us at [dev@stopcoronavirus.tech](mailto:dev@stopcoronavirus.tech)

We will add your server as a bootstrap node for :

- the dedicated IPFS network deployed for [www.stopcoronavirus.tech](https://www.stopcoronavirus.tech)
- [entraide.stopcoronavirus.tech](https://gitlab.com/stopcoronavirus/orbit-web)
- [docs.stopcoronavirus.tech](https://gitlab.com/stopcoronavirus/peer-pad)
- [peer-base pinner service](https://gitlab.com/stopcoronavirus/peer-base)

## Step 6b : Joining the decentralized network as a peer node

You should be able to remove the following services under ```docker-compose.yml``` : ```ipfs-gateway-wss```, ```wss-star-rdv```. No request should reach them if they're not part of the bootstrap list.


# Help and Contributions Welcome

This platform was configured and deployed very rapidly, as a one-man job.  
I am sure you have ideas to improve it.  

Please get in touch [dev@stopcoronavirus.tech](mailto:dev@stopcoronavirus.tech) or [submit an issue.](https://gitlab.com/stopcoronavirus/infra-follow/-/issues/new)

**Some ideas**

- generate iframe code so it can be integrated easily on other websites
- facilitate sharing by generating social networks messages
- make it ready to deploy for other country, areas (was done for France at the city and departement level)
- make it easy to deploy at a more granular level if scaling becomes an issue (one deployment per zone)
- translate the platform
- make the p2p chat works on iOS (seems to be broken)
- use a fork of https://raw.chat instead of [orbit.chat](https://github.com/orbitdb/orbit-web)

# Attribution

All credits go to the awesome IPFS ecosystem

- [ipfs.io](https://ipfs.io/)
- [orbit.chat](https://github.com/orbitdb/orbit-web)
- [peerpad.net](https://github.com/peer-base/peer-pad)
- [peerbase](https://github.com/peer-base/peer-base)
- [libp2p](https://libp2p.io/)