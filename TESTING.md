# Testing 

## Testing services from external host

```
export SERVER_NAME=alpha-01

# PINNER
curl http://pbpinner.${SERVER_NAME}.bootstrap.stopcoronavirus.tech/health

# WSS
websocat wss://ipfs-gateway-wss.${SERVER_NAME}.bootstrap.stopcoronavirus.tech

# IPFS gateway
curl https://ipfs-gateway.${SERVER_NAME}.bootstrap.stopcoronavirus.tech/ipfs/QmP48ZRbtoYQQf1pCSvEw8JgsrvRCVeemoHNMnYBzcoVyn

# RDV Server
curl https://rdv.${SERVER_NAME}.bootstrap.stopcoronavirus.tech
```

## Testing WSS endpoint

### on MacOS

```
brew install websocat
websocat wss://ipfs-gateway-wss.alpha-01.bootstrap.stopcoronavirus.tech
```

### Online

https://www.websocket.org/echo.html
